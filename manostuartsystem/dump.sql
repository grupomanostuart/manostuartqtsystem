-- MySQL dump 10.13  Distrib 5.5.37, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: loja
-- ------------------------------------------------------
-- Server version	5.5.37-0ubuntu0.13.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Carrinho`
--

DROP TABLE IF EXISTS `Carrinho`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Carrinho` (
  `Login` varchar(50) NOT NULL,
  `Nome` varchar(80) NOT NULL,
  `Marca` varchar(80) NOT NULL,
  `Qnt` int(11) DEFAULT NULL,
  PRIMARY KEY (`Login`,`Nome`,`Marca`),
  KEY `Nome` (`Nome`,`Marca`),
  CONSTRAINT `Carrinho_ibfk_1` FOREIGN KEY (`Login`) REFERENCES `Usuario` (`Login`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Carrinho_ibfk_2` FOREIGN KEY (`Nome`, `Marca`) REFERENCES `Produto` (`Nome`, `Marca`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Carrinho`
--

LOCK TABLES `Carrinho` WRITE;
/*!40000 ALTER TABLE `Carrinho` DISABLE KEYS */;
INSERT INTO `Carrinho` VALUES ('abc@def.com','Whey Gold 100%','Optimum Nutrition',2),('jkl@def.com','100% Pure Whey Protein','Probiótica',1),('jkl@def.com','Óleo de Coco','Body Action',1),('jkl@def.com','Whey Gold 100%','Optimum Nutrition',1);
/*!40000 ALTER TABLE `Carrinho` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Compra`
--

DROP TABLE IF EXISTS `Compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Compra` (
  `id` int(11) DEFAULT NULL,
  `Login` varchar(50) NOT NULL,
  `Preco` int(11) DEFAULT NULL,  
  `DataC` varchar(80) DEFAULT NULL,
  `Endereco` varchar (512) NOT NULL,
  `Pago` boolean DEFAULT FALSE,
  PRIMARY KEY (`id`),  
  CONSTRAINT `Compra_ibfk_1` FOREIGN KEY (`Login`) REFERENCES `Usuario` (`Login`) ON DELETE NO ACTION ON UPDATE NO ACTION  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Compra`
--

LOCK TABLES `Compra` WRITE;
/*!40000 ALTER TABLE `Compra` DISABLE KEYS */;
INSERT INTO `Compra` VALUES (1,'abc@def.com',20000,'2014-05-01', 'Rua: Machado de Assis\nBairro: Liberdade\nNumero: 33\nComplemento: 12\nCEP: 123456789\nCidade: Foo\nEstado: Bar', FALSE);
/*!40000 ALTER TABLE `Compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Endereco`
--

DROP TABLE IF EXISTS `Endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Endereco` (
  `Login` varchar(50) NOT NULL,
  `CEP` varchar(8) NOT NULL,
  `Rua` varchar(50) NOT NULL,
  `Numero` int(11) NOT NULL,
  `Complemento` varchar(50) DEFAULT NULL,
  `Bairro` varchar(50) NOT NULL,
  `Estado` varchar(2) NOT NULL,
  `Cidade` varchar(20) NOT NULL,
  PRIMARY KEY (`Login`),
  CONSTRAINT `Endereco_ibfk_1` FOREIGN KEY (`Login`) REFERENCES `Usuario` (`Login`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Endereco`
--

LOCK TABLES `Endereco` WRITE;
/*!40000 ALTER TABLE `Endereco` DISABLE KEYS */;
INSERT INTO `Endereco` VALUES ('abc@def.com','12345678','RUA1',1,'','BAIRRO1','SP','CIDADE1'),('ghi@def.com','98765432','RUA2',34,'','BAIRRO7','AC','CIDADE1'),('jkl@def.com','98765432','RUA2',209,'ao lado do postinho','BAIRRO7','AC','CIDADE1');
/*!40000 ALTER TABLE `Endereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Fretes`
--

DROP TABLE IF EXISTS `Fretes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Fretes` (
  `Peso` int(11) NOT NULL,
  `Preco` int(11) NOT NULL,
  `Dim_x` int(11) NOT NULL,
  `Dim_y` int(11) NOT NULL,
  `Dim_z` int(11) NOT NULL,
  `Data` date NOT NULL,
  PRIMARY KEY (`Peso`,`Preco`,`Dim_x`,`Dim_y`,`Dim_z`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Fretes`
--

LOCK TABLES `Fretes` WRITE;
/*!40000 ALTER TABLE `Fretes` DISABLE KEYS */;
INSERT INTO `Fretes` VALUES (300,7,20,30,10,'2014-04-07'),(300,12,60,50,25,'2014-04-07'),(500,13,20,30,10,'2014-04-07'),(500,16,60,50,25,'2014-04-07'),(1000,18,20,30,10,'2014-04-07'),(1000,21,60,50,25,'2014-04-07'),(2000,23,20,30,10,'2014-04-07'),(2000,25,60,50,25,'2014-04-07');
/*!40000 ALTER TABLE `Fretes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Produto`
--

DROP TABLE IF EXISTS `Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto` (
  `Nome` varchar(80) NOT NULL,
  `Marca` varchar(80) NOT NULL,
  `Categoria` varchar(80) NOT NULL,
  `Foto` varchar(80) NOT NULL,
  `Estoque` int(11) NOT NULL,
  `Preco` int(11) NOT NULL,
  `Peso` int(11) NOT NULL,
  `Dim_x` int(11) NOT NULL,
  `Dim_y` int(11) NOT NULL,
  `Dim_z` int(11) NOT NULL,
  PRIMARY KEY (`Nome`,`Marca`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Produto`
--

LOCK TABLES `Produto` WRITE;
/*!40000 ALTER TABLE `Produto` DISABLE KEYS */;
INSERT INTO `Produto` VALUES ('100% Pure Whey Protein','Probiótica','Massa Muscular','./pics/939FG1.jpg',50,9891,900,70,25,25),('Óleo de Coco','Body Action','Emagrecimento','./pics/2031FG1.jpg',50,1791,300,20,7,7),('Whey Gold 100%','Optimum Nutrition','Massa Muscular','./pics/1034FG1.jpg',50,16196,1500,50,30,30);
/*!40000 ALTER TABLE `Produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Promocao_cliente`
--

DROP TABLE IF EXISTS `Promocao_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Promocao_cliente` (
  `Login` varchar(50) NOT NULL,
  `Desconto` int(11) NOT NULL,
  KEY `Login` (`Login`),
  CONSTRAINT `Promocao_cliente_ibfk_1` FOREIGN KEY (`Login`) REFERENCES `Usuario` (`Login`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Promocao_cliente`
--

LOCK TABLES `Promocao_cliente` WRITE;
/*!40000 ALTER TABLE `Promocao_cliente` DISABLE KEYS */;
INSERT INTO `Promocao_cliente` VALUES ('abc@def.com',5),('ghi@def.com',5);
/*!40000 ALTER TABLE `Promocao_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Promocoes_produto`
--

DROP TABLE IF EXISTS `Promocoes_produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Promocoes_produto` (
  `Nome` varchar(80) NOT NULL,
  `Marca` varchar(80) NOT NULL,
  `Desconto` int(11) NOT NULL,
  KEY `Nome` (`Nome`,`Marca`),
  CONSTRAINT `Promocoes_produto_ibfk_1` FOREIGN KEY (`Nome`, `Marca`) REFERENCES `Produto` (`Nome`, `Marca`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Promocoes_produto`
--

LOCK TABLES `Promocoes_produto` WRITE;
/*!40000 ALTER TABLE `Promocoes_produto` DISABLE KEYS */;
INSERT INTO `Promocoes_produto` VALUES ('100% Pure Whey Protein','Probiótica',15);
/*!40000 ALTER TABLE `Promocoes_produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rank_Produto`
--

DROP TABLE IF EXISTS `Rank_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rank_Produto` (
  `Nome` varchar(80) NOT NULL,
  `Marca` varchar(80) NOT NULL,
  `Qnt_vendida` int(11) NOT NULL,
  `Qnt_buscada` int(11) NOT NULL,
  PRIMARY KEY (`Nome`,`Marca`),
  CONSTRAINT `Rank_Produto_ibfk_1` FOREIGN KEY (`Nome`, `Marca`) REFERENCES `Produto` (`Nome`, `Marca`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rank_Produto`
--

LOCK TABLES `Rank_Produto` WRITE;
/*!40000 ALTER TABLE `Rank_Produto` DISABLE KEYS */;
INSERT INTO `Rank_Produto` VALUES ('100% Pure Whey Protein','Probiótica',9,9),('Óleo de Coco','Body Action',5,21),('Whey Gold 100%','Optimum Nutrition',7,8);
/*!40000 ALTER TABLE `Rank_Produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Usuario`
--

DROP TABLE IF EXISTS `Usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Usuario` (
  `Login` varchar(50) NOT NULL,
  `Senha` varchar(30) NOT NULL,
  `Nome` varchar(50) NOT NULL,
  `CPF` varchar(15) NOT NULL,
  `RG` varchar(13) NOT NULL,
  `Nascimento` date NOT NULL,
  `Sexo` varchar(1) NOT NULL,
  `admin` int(11) NOT NULL,
  PRIMARY KEY (`Login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Usuario`
--

LOCK TABLES `Usuario` WRITE;
/*!40000 ALTER TABLE `Usuario` DISABLE KEYS */;
INSERT INTO `Usuario` VALUES ('abc@def.com','senha','abc','12345678910','12345678','1990-01-01','M', 0),('ghi@def.com','senha','ghi','98765432164','98765432','1990-01-02','F', 0),('jkl@def.com','senha','jkl','19283764553','91374682','1990-01-03','M', 0),('mno@def.com','senha','Ed','7654321','666666','1991-01-01','M', 1);
/*!40000 ALTER TABLE `Usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-21 22:15:18
