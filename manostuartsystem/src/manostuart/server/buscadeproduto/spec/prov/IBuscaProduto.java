package manostuart.server.buscadeproduto.spec.prov;
import java.util.ArrayList;

import manostuart.shared.data.Produto;


public interface IBuscaProduto{

	public ArrayList<Produto> buscarProdutoNome ( String prod ); 
	public ArrayList<Produto> buscarProdutoMarca ( String marca );
	public ArrayList<Produto> buscarProdutoCategoria ( String categoria );

}