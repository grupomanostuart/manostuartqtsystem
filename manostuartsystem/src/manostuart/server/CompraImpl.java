package manostuart.server;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpSession;

import manostuart.client.CompraService;
import manostuart.shared.data.*;
import manostuart.server.bancodedados.Bd;
import manostuart.server.gerentedeusuarios_sistema.impl.IGerenteUsuarioAdapter;
import manostuart.server.cadastrodecompras_sistema.impl.ICadastroCompraAdapter;
import manostuart.server.carrinho_sistema.impl.ICarrinhoAdapter;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial") //no java, u dont get your ID serials
public class CompraImpl  extends RemoteServiceServlet implements CompraService  
{	
	public Compra comprar(ArrayList<Produto> prod, int total)
	{
		IGerenteUsuarioAdapter usrAdapter = new IGerenteUsuarioAdapter();
		ICadastroCompraAdapter compraAdapter = new ICadastroCompraAdapter();
		ICarrinhoAdapter carAdapter = new ICarrinhoAdapter();
		Compra c = new Compra();
		Usuario usr = new Usuario();
		Date date = new Date();
		String endereco = new String();
		ResultSet rs = null;
		int id = 0;
		
		HttpSession session = this.getThreadLocalRequest().getSession();
		// get Usuario e endereco
		if (session.getAttribute("login") == null)
			return null;
		usr = usrAdapter.getUsuario((String)session.getAttribute("login"));		
		endereco = usrAdapter.getEndereco((String)session.getAttribute("login"));
		//get ID
		rs = Bd.query("SELECT MAX(id) FROM compra");
		try {
			if (rs != null && rs.next())
				id = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		id += 1;
		session.setAttribute("id", id);
		
		//cadastrar compra
		c.setid(id);
		c.setprods(prod);
		c.setvalor(total);
		c.setusr(usr);
		c.setdata(date);
		c.setendereco(endereco);
		c.setpago(false);		
		compraAdapter.cadastrarCompra(c);
		
		//drop carrinho
		carAdapter.dropCarrinho(usr.getlogin());

		return c;
	}

	
	public Boolean setPagamentoEfetuado(String endereco) 
	{		
		ICadastroCompraAdapter compraAdapter = new ICadastroCompraAdapter();		
		HttpSession session = this.getThreadLocalRequest().getSession();		
		
		Boolean pagar = compraAdapter.setPago((int)session.getAttribute("id"), true);
		Boolean alterarEndereco = compraAdapter.setEndereco((int)session.getAttribute("id"), endereco);
		
		return pagar && alterarEndereco;
	}
	
	public ArrayList<Compra> getComprasHistorico()
	{
		HttpSession session = this.getThreadLocalRequest().getSession();
		if (session.getAttribute("login") == null)
			return null;
		
		ArrayList<Compra> listCompras = new ArrayList<Compra>();		
		String login = new String();
		
		login = (String)session.getAttribute("login");		
		String cmd = "SELECT * FROM Compra WHERE Login='" + login + "'" ;		
		ResultSet rs = Bd.query(cmd);
		try {
			while (rs.next())
			{
				Compra c = new Compra();
				c.setid(rs.getInt(1));
				c.setvalor(rs.getInt(3));				
				c.setdata(rs.getString(4));
				c.setendereco(rs.getString(5));
				c.setpago(rs.getBoolean(6));
				listCompras.add(c);				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listCompras;
		
	}
}
