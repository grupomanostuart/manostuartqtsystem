package manostuart.server.gerentedeusuarios.spec.prov;
import manostuart.shared.data.Usuario;


public interface IGerenteUsuario{

	public boolean inserirUsuario ( Usuario usr ); 
	public Usuario getUsuario ( String login );
	public boolean updateUsuario ( Usuario oldUsr, Usuario newUsr );
	public String getEndereco(String login);
}
