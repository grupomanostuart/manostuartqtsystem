package manostuart.server.carrinho_sistema.impl;

import manostuart.server.bancodedados.Bd;
import manostuart.server.sistema.spec.req.ICarrinho;
import manostuart.shared.data.Produto;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;


public class ICarrinhoAdapter implements ICarrinho{

	public int calcularTotal ( String usr ){
		
		int total = 0;
		String cmd;
		ResultSet rs, rss;

		cmd = "select * FROM Carrinho WHERE Carrinho.Login='" + usr + "';";

		rs = Bd.query(cmd);

		try{
			while(rs.next()){
				cmd = "select Preco FROM Produto WHERE Produto.Nome='" + rs.getString(2) +
						"' AND Produto.Marca='" + rs.getString(3) + "';";
				rss = Bd.query(cmd);
				rss.next();
				total += (rss.getInt(1)*rs.getInt(4));
			}
		}
		catch(SQLException ex){
			ex.printStackTrace();
		}

		return total;
	} 
	public boolean adicionarProduto ( Produto prod, String usr, int qnt ){
		
		String cmd;
		ResultSet rs;
		String nome, marca;
		int quantidade;
		boolean state=false;
		
		nome = prod.getnome();
		marca = prod.getmarca();
		quantidade = qnt;
		
		cmd = "select * FROM Carrinho WHERE Login='" + usr + "' AND Nome='" + 
		nome + "' AND Marca='" + marca + "';";
		rs = Bd.query(cmd);
		try {
			if(rs.next()){
				quantidade += rs.getInt(4);
				cmd = "update Carrinho set Qnt=" + quantidade + " WHERE Login='" + 
				usr + "' AND Nome='" + nome + "' AND Marca='" + marca +"';";
				state = Bd.update(cmd);
			}
			else{
				cmd = "insert into Carrinho VALUES ('" + usr + "', '" +
				nome + "', '" + marca + "', " + quantidade + ");";
				state = Bd.update(cmd);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return state;
	} 

	public ArrayList<Produto> getCarrinho (String usr){

		String cmd;
		ResultSet rs, rss;
		Produto p;
		ArrayList<Produto> lista = new ArrayList<Produto>();

		cmd = "select * FROM Carrinho WHERE Carrinho.Login='" + usr + "';";

		rs = Bd.query(cmd);

		try{
			while(rs.next()){
				p = new Produto();			
				p.setnome(rs.getString(2));
				p.setmarca(rs.getString(3));
				cmd = "select * FROM Produto WHERE Produto.Nome='" + p.getnome() + "' AND Produto.Marca='" +
				p.getmarca() + "';";
				rss = Bd.query(cmd);
				rss.next();
				p.setcategoria(rss.getString(3));
				p.setURLfoto(rss.getString(4));
				p.setestoque(rss.getInt(5));
				p.setpreco(rss.getInt(6));
				p.setpeso(rss.getInt(7));
				p.setdim_x(rss.getInt(8));
				p.setdim_y(rss.getInt(9));
				p.setdim_z(rss.getInt(10));
				p.setqnt(rs.getInt(4));
				lista.add(p);
			}
		}
		catch(SQLException ex){		
			ex.printStackTrace();
		}

			
		return lista;
	}
	public boolean dropCarrinho(String usr) {
		
		String cmd;
		
		cmd = "delete FROM Carrinho WHERE Login='" + usr + "';";
		
		return Bd.update(cmd);
	}
	public boolean dropItemCarrinho(String usr, Produto p) {
		
		String cmd;
		
		cmd = "delete FROM Carrinho Where Login='" + usr + "' AND Nome='" +
		p.getnome() + "' AND Marca='" + p.getmarca() + "';";
		
		return Bd.update(cmd);
	}
	public boolean addListaCarrinho(String usr, ArrayList<Produto> p) {
		
		String cmd;
		ResultSet rs;
		String nome, marca;
		int quantidade;
		
		for(Produto prod: p){
			nome = prod.getnome();
			marca = prod.getmarca();
			quantidade = prod.getqnt();
			
			cmd = "select * FROM Carrinho WHERE Login='" + usr + "' AND Nome='" + 
			nome + "' AND Marca='" + marca + "';";
			rs = Bd.query(cmd);
			try {
				if(rs.next()){
					quantidade += rs.getInt(4);
					cmd = "update Carrinho set Qnt=" + quantidade + " WHERE Login='" + 
					usr + "' AND Nome='" + nome + "' AND Marca='" + marca +"';";
					Bd.update(cmd);
				}
				else{
					cmd = "insert into Carrinho VALUES ('" + usr + "', '" +
					nome + "', '" + marca + "', " + quantidade + ");";
					Bd.update(cmd);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		}
		
		return true;
	}

}