package manostuart.server.sistema.spec.req;
import manostuart.shared.data.Produto;
import java.util.ArrayList;


public interface ICarrinho{

	public int calcularTotal ( String usr ); 
	public boolean adicionarProduto ( Produto prod, String usr, int qnt ); 
	public ArrayList<Produto> getCarrinho (String usr);
	public boolean dropCarrinho(String usr);
	public boolean dropItemCarrinho(String usr, Produto p);
	public boolean addListaCarrinho(String usr, ArrayList<Produto> p);
}
