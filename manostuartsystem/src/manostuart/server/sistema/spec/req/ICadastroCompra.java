package manostuart.server.sistema.spec.req;
import manostuart.shared.data.Compra;


public interface ICadastroCompra{

	public boolean cadastrarCompra ( Compra compra ); 
	public Compra getCompra(int id);
	public boolean setPago(int id, boolean state);
	public boolean gerarComprovante ( String com_id ); 
}