package manostuart.server.sistema.spec.req;
import manostuart.shared.data.Produto;


public interface ICadastroProduto{

	public boolean cadastroProduto ( Produto prod ); 
	public boolean buscarProduto ( Produto prod ); 
}