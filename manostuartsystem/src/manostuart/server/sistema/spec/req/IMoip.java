package manostuart.server.sistema.spec.req;
import manostuart.shared.data.Usuario;


public interface IMoip{

	public boolean efetuarPagamentoBoleto ( int valor, Usuario usr ); 
	public boolean efetuarPagamentoCartao(String cartao);
}
