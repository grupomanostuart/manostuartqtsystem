package manostuart.server.sistema.spec.prov;
import manostuart.shared.data.*;

public interface ISistema{

	public boolean promoInfo (  ); 
	public boolean calcularFrete ( String endereco ); 
	public boolean confirmarPagamento ( Usuario usr ); 
	public boolean adicionarUsuario ( String usr_id, String nome, String CPF, String senha ); 
}