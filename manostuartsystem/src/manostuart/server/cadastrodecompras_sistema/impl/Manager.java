package manostuart.server.cadastrodecompras_sistema.impl;

import java.util.*;

class Manager implements IManager{

	Map<String, Object> reqInterfaceMap = new HashMap<String, Object>();

	public String[] getProvidedInterfaces(){
	   List<String> provInterfaceList = new ArrayList<String>();
	   provInterfaceList.add("ICadastroCompra");
	     
	   
	   return convertListToArray(provInterfaceList);
	}
	
	public String[] getRequiredInterfaces(){
	
		return convertListToArray(reqInterfaceMap.keySet());
	}
	
	public Object getProvidedInterface(String name){

	   if (name.equals("ICadastroCompra")){
	   		return new ICadastroCompraAdapter();
	   }
	   
	   return null;
	}
	
	public void setRequiredInterface(String name, Object adapter){
		reqInterfaceMap.put(name,adapter);
	}
	
	public Object getRequiredInterface(String name){
	   return reqInterfaceMap.get(name);
	}

	private String[] convertListToArray(Collection<String> stringCollection){
		String[] stringArray = new String[stringCollection.size()];
		int i=0;
		for (Iterator<String> iter=stringCollection.iterator();iter.hasNext();){
			stringArray[i] = (String)iter.next();
			i++;
		}
		return stringArray;
	}
}



