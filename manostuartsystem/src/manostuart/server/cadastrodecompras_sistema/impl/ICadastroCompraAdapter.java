   
package manostuart.server.cadastrodecompras_sistema.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import manostuart.server.bancodedados.Bd;
import manostuart.server.sistema.spec.req.ICadastroCompra;
import manostuart.server.gerentedeusuarios_sistema.impl.IGerenteUsuarioAdapter;
import manostuart.shared.data.*;

public class ICadastroCompraAdapter implements ICadastroCompra{

	public boolean cadastrarCompra ( Compra compra ){
		
		String cmd, login;
		ArrayList<Produto> l;
		int pago;
		int preco = compra.getvalor();
		
		if(compra.getpago())
			pago = 1;
		else
			pago = 0;
		login = compra.getusr().getlogin();
		l = compra.getprods();
		
		
		
		cmd = "insert into Compra VALUES(" + compra.getid() + ", '" + login + "', " +
		preco + ", '" + compra.getdata() + "', '" + compra.getendereco() + "', " + pago + ");";		
		Bd.update(cmd);

		
		return true;	
	} 
	
	public boolean gerarComprovante ( String com_id ){
		return false;
	}
	
	public Compra getCompra(int id) {
		
		String cmd;
		ResultSet rs, rss;
		IGerenteUsuarioAdapter gusr = new IGerenteUsuarioAdapter();
		Compra c = new Compra();
		int valor = 0;
		
		
		cmd = "select * FROM Compra WHERE id=" + id + ";";
		rs = Bd.query(cmd);
		try {
			if(rs.next()){
				c.setusr(gusr.getUsuario(rs.getString(2)));
				c.setid(rs.getInt(1));
				if(rs.getInt(9) == 1)
					c.setpago(true);
				else
					c.setpago(false);
				c.setdata(rs.getDate(7));
				c.setendereco(rs.getString(8));
				do{
					valor += rs.getInt(6)*rs.getInt(5);
					Produto p = new Produto();
					cmd = "select * FROM Produto WHERE Nome='" + rs.getString(3) +
					"' AND Marca='" + rs.getString(4) + "';";
					rss = Bd.query(cmd);
					rss.next();
					p.setnome(rss.getString(1));
					p.setmarca(rss.getString(2));
					p.setcategoria(rss.getString(3));
					p.setURLfoto(rss.getString(4));
					p.setestoque(rss.getInt(5));
					p.setpreco(rss.getInt(6));
					p.setpeso(rss.getInt(7));
					p.setdim_x(rss.getInt(8));
					p.setdim_y(rss.getInt(9));
					p.setdim_z(rss.getInt(10));
					p.setqnt(rs.getInt(5));
					c.addProdLista(p);
				}while(rs.next());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		c.setvalor(valor);
		
		return c;
	}
	
	public boolean setPago(int id, boolean state) {
		
		String cmd;
		int pago;
		
		if(state)
			pago = 1;
		else
			pago = 0;
		
		cmd = "update Compra SET Pago=" + pago + " WHERE id=" + id + ";";		
		return Bd.update(cmd);
	}
	
	public boolean setEndereco(int id, String endr)
	{
		String cmd;		
		cmd = "update Compra SET Endereco='" + endr + "' WHERE id=" + id + ";";
		return Bd.update(cmd);
	}

}