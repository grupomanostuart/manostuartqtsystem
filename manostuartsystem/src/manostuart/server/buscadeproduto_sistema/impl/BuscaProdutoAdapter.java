package manostuart.server.buscadeproduto_sistema.impl;
import manostuart.server.bancodedados.Bd;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import manostuart.server.buscadeproduto.spec.prov.IBuscaProduto;
import manostuart.shared.data.Produto;

public class BuscaProdutoAdapter implements IBuscaProduto{

	public ArrayList<Produto> buscarProdutoNome ( String prod ){

		String cmd;
		ResultSet rs;
		Produto p;
		ArrayList<Produto> lista = new ArrayList<Produto>();

		//cmd = "select * FROM Produto WHERE Produto.nome = '" + prod + "';";
		
		//Adaptei a query para procurar por partes de string
		cmd = "select * FROM Produto WHERE Produto.nome LIKE '" + "%" + prod + "%" +"';";
		
		rs = Bd.query(cmd);

		try{
			while(rs.next()){
				//TODO percorrer o resultado
				//http://zetcode.com/db/mysqljava/	
				p = new Produto();			
				p.setnome(rs.getString(1));
				p.setmarca(rs.getString(2));
				p.setcategoria(rs.getString(3));
				p.setURLfoto(rs.getString(4));
				p.setestoque(rs.getInt(5));
				p.setpreco(rs.getInt(6));
				p.setpeso(rs.getInt(7));
				p.setdim_x(rs.getInt(8));
				p.setdim_y(rs.getInt(9));
				p.setdim_z(rs.getInt(10));
				lista.add(p);
			}
		} catch(SQLException ex) { 
			ex.printStackTrace();
		}

		return lista;
	}

	public ArrayList<Produto> buscarProdutoMarca ( String marca ){

		String cmd;
		ResultSet rs;
		Produto p;
		ArrayList<Produto> lista = new ArrayList<Produto>();

		cmd = "select * FROM Produto WHERE Produto.marca LIKE '" + "%" + marca + "%" + "';";
		rs = Bd.query(cmd);

		try{
			while(rs.next()){
				//TODO percorrer o resultado
				//http://zetcode.com/db/mysqljava/
				p = new Produto();
				p.setnome(rs.getString(1));
				p.setmarca(rs.getString(2));
				p.setcategoria(rs.getString(3));
				p.setURLfoto(rs.getString(4));
				p.setestoque(rs.getInt(5));
				p.setpreco(rs.getInt(6));
				p.setpeso(rs.getInt(7));
				p.setdim_x(rs.getInt(8));
				p.setdim_y(rs.getInt(9));
				p.setdim_z(rs.getInt(10));
				lista.add(p);
			}
		} catch(SQLException ex) {}

		return lista;
	}

	public ArrayList<Produto> buscarProdutoCategoria ( String categoria ){

		String cmd;
		ResultSet rs;
		Produto p;
		ArrayList<Produto> lista = new ArrayList<Produto>();

		cmd = "select * FROM Produto WHERE Produto.categoria LIKE '" + "%" + categoria + "%" + "';";
		rs = Bd.query(cmd);

		try{
			while(rs.next()){
				//TODO percorrer o resultado
				//http://zetcode.com/db/mysqljava/
				p = new Produto();
				p.setnome(rs.getString(1));
				p.setmarca(rs.getString(2));
				p.setcategoria(rs.getString(3));
				p.setURLfoto(rs.getString(4));
				p.setestoque(rs.getInt(5));
				p.setpreco(rs.getInt(6));
				p.setpeso(rs.getInt(7));
				p.setdim_x(rs.getInt(8));
				p.setdim_y(rs.getInt(9));
				p.setdim_z(rs.getInt(10));
				lista.add(p);
			}
		} catch(SQLException ex) {}

		return lista;
	}
}