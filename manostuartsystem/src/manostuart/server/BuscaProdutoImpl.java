package manostuart.server;

import java.util.ArrayList;

import manostuart.client.BuscaProdutoService;
import manostuart.server.bancodedados.Bd;
import manostuart.server.buscadeproduto_sistema.impl.*;
import manostuart.shared.data.Produto;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial") //no java, u dont get your ID serials
public class BuscaProdutoImpl extends RemoteServiceServlet implements BuscaProdutoService 
{	
	public ArrayList<Produto> BuscaProduto(String str, int opcao)
	{
		Bd.start_bd();
		BuscaProdutoAdapter busca = new BuscaProdutoAdapter();
		ArrayList<Produto> listaProdutos;
		if (opcao == 0) //busca por nome
		{
			listaProdutos = busca.buscarProdutoNome(str);
		}
		else if (opcao == 1) // busca por marca
		{
			listaProdutos = busca.buscarProdutoMarca(str);		
		}
		else //busca por categoria
		{
			listaProdutos = busca.buscarProdutoCategoria(str);		
		}
		//System.out.println(listaProdutos.get(0).getnome());
			
		return listaProdutos;
	}
}
