package manostuart.server;

import manostuart.client.CadastroProdutoService;
import manostuart.server.bancodedados.Bd;
import manostuart.server.cadastrodeproduto_sistema.impl.*;

import manostuart.shared.data.Produto;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial") //no java, u dont get your ID serials
public class CadastroProdutoImpl extends RemoteServiceServlet implements CadastroProdutoService 
{	
	public Boolean CadastroProduto(String nome, String marca, String categoria, String url,
			 int estoque, int preco, int peso, int dim_x, int dim_y, int dim_z)
	{
		Bd.start_bd();
		CadastroProdutoAdapter cadastro = new CadastroProdutoAdapter ();
		
		Produto prod = new Produto();
		prod.setcategoria(categoria);
		prod.setdim_x(dim_x);
		prod.setdim_y(dim_y);
		prod.setdim_z(dim_z);
		prod.setestoque(estoque);
		prod.setmarca(marca);
		prod.setnome(nome);
		prod.setpeso(peso);
		prod.setpreco(preco);
		prod.setURLfoto(url);
		
		System.out.println("Chamando bd");
		boolean insertion_success = cadastro.cadastroProduto(prod);
		System.out.println("Bd eh noix");
		return insertion_success;	
	}
}