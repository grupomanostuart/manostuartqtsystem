package manostuart.server.calculadorfrete_sistema.spec.req;

import manostuart.shared.data.Produto;

public interface ICorreio{

	public boolean buscarEndereco ( String endereco ); 
	public int calcularFrete(Produto p);
}