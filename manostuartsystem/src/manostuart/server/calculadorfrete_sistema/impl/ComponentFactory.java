package manostuart.server.calculadorfrete_sistema.impl;

import manostuart.server.calculadorfrete_sistema.impl.IManager;

public class ComponentFactory {

	private static IManager manager = null;

	public static IManager createInstance(){
	
		if (manager==null)
			manager = new Manager();
		
		return manager;
	}
}



