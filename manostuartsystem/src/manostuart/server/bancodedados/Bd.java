package manostuart.server.bancodedados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.log.LogQuery.Version;

public class Bd {    

    static Connection con = null;
    static Statement st = null;
    static ResultSet rs = null;
    static boolean conectado = false;

    public static void start_bd() {
    	
    	// ------------------------------ IMPORTANT ------------------------------
    	// The variables below are the variables to connect to the mySQL
    	// Change it to connect to your mySQL server
    	
    	// Example:
    	
    	// "url": should be left with the default value, unless you change the port
    	// "db": should be changed to the name of your Schema
    	// "driver": should be left with the default value
    	// "user" and "pass": change to your root user login/password
    	
    	// ------------------------------ ********* ------------------------------
        
        String url          = "jdbc:mysql://localhost:3306/";
        String db           = "manostuartsystem";
        String driver       = "com.mysql.jdbc.Driver";
        String user         = "root";
        String password     = "pass";	

        if(conectado == false){
        	try {
                Class.forName(driver).newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            try {
                con = DriverManager.getConnection(url+db, user, password);
                st = con.createStatement();
            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(Version.class.getName());
                lgr.log(Level.SEVERE, ex.getMessage(), ex);

            }
        }
    }
    
    public static ResultSet query(String query){
        
        ResultSet rs = null;
        PreparedStatement ps;

        try {
        	ps = con.prepareStatement(query);
            rs = ps.executeQuery();
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Bd.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            return null;
        }

        return rs;
    }  

    public static boolean update(String cmd){
        
        try{
            st.executeUpdate(cmd);
        }
        catch (SQLException ex){
            Logger lgr = Logger.getLogger(Bd.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
    		System.out.println(ex.getMessage());
            return false;
        }

        return true;
    }
}
