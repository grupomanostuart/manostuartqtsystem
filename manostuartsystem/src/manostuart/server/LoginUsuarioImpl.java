package manostuart.server;

import manostuart.client.LoginUsuarioService;
import manostuart.server.bancodedados.Bd;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import javax.servlet.http.HttpSession;

import manostuart.server.gerentedeusuarios_sistema.impl.*;
import manostuart.shared.data.Usuario;

@SuppressWarnings("serial") //no java, u dont get your ID serials
public class LoginUsuarioImpl extends RemoteServiceServlet implements LoginUsuarioService 
{	
	public Boolean LoginUsuario(String name, String psw)
	{
		Bd.start_bd();
		
		GerenteUsuarioAdapter login = new GerenteUsuarioAdapter();
		Usuario user = login.getUsuario(name);
		
		if (user.getsenha() == null)
			return false;
		if (!user.getsenha().contentEquals(psw))
			return false;

		HttpSession session = this.getThreadLocalRequest().getSession();
		session.setAttribute("login", name);
		session.setAttribute("logged", true);
		
		session.setAttribute("manager", user.isadmin());
		
		return true;
	}
	
	public Boolean CheckLogin ()
	{
		HttpSession session = this.getThreadLocalRequest().getSession();
		if (session.getAttribute("logged") == null)
			return false;
		return (boolean)session.getAttribute("logged");
	}
	
	public Boolean CheckManager ()
	{
		HttpSession session = this.getThreadLocalRequest().getSession();
		if (session.getAttribute("manager") == null)
			return false;
		return (boolean)session.getAttribute("manager");
	}
	

	public Boolean Logout ()
	{
		HttpSession session = this.getThreadLocalRequest().getSession();
		session.setAttribute("logged", false);
		session.setAttribute("manager", false);
		session.setAttribute("login", null);
		
		return true;
	}
}