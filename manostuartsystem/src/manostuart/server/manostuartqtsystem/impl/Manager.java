package manostuart.server.manostuartqtsystem.impl;

import java.util.*;

import manostuart.server.manostuartqtsystem.spec.prov.IManager;


class Manager implements IManager{

	Map<String, Object> reqInterfaceMap = new HashMap<String, Object>();

	Manager(){
	
		// Instantiate Components and Connectors
		
		manostuart.server.sistema.spec.prov.IManager managerSistema =
		manostuart.server.sistema.impl.ComponentFactory.createInstance();
		
		manostuart.server.moip_sistema.impl.IManager managerMoip_Sistema =
		manostuart.server.moip_sistema.impl.ComponentFactory.createInstance();
		
		manostuart.server.carrinho_sistema.impl.IManager managerCarrinho_Sistema =
		manostuart.server.carrinho_sistema.impl.ComponentFactory.createInstance();
		
		manostuart.server.calculadorfrete_sistema.impl.IManager managerCalculadorFrete_Sistema =
		manostuart.server.calculadorfrete_sistema.impl.ComponentFactory.createInstance();
		
		manostuart.server.gerentedeusuarios_sistema.impl.IManager managerCadastroDeUsuarios_Sistema =
		manostuart.server.gerentedeusuarios_sistema.impl.ComponentFactory.createInstance();
		
		manostuart.server.cadastrodeproduto_sistema.impl.IManager managerCadastroDeProduto_Sistema =
		manostuart.server.cadastrodeproduto_sistema.impl.ComponentFactory.createInstance();
		
		manostuart.server.cadastrodecompras_sistema.impl.IManager managerCadastroDeCompras_Sistema =
		manostuart.server.cadastrodecompras_sistema.impl.ComponentFactory.createInstance();
		
		manostuart.server.cadastrodecompras.spec.prov.IManager managerCadastroDeCompras =
		manostuart.server.cadastrodecompras.impl.ComponentFactory.createInstance();
		
		manostuart.server.cadastrodeproduto.spec.prov.IManager managerCadastroDeProduto =
		manostuart.server.cadastrodeproduto.impl.ComponentFactory.createInstance();
		
//		manostuart.server.gerentedeusuarios.spec.prov.IManager managerCadastroDeUsuarios =
//		manostuart.server.gerentedeusuarios.impl.ComponentFactory.createInstance();
		
		manostuart.server.correio.spec.prov.IManager managerCorreio =
		manostuart.server.correio.impl.ComponentFactory.createInstance();
		
		manostuart.server.correio_calculadorfrete.impl.IManager managerCorreio_CalculadorFrete =
		manostuart.server.correio_calculadorfrete.impl.ComponentFactory.createInstance();
		
		manostuart.server.carrinho.spec.prov.IManager managerCarrinho =
		manostuart.server.carrinho.impl.ComponentFactory.createInstance();
		
		manostuart.server.moip.spec.prov.IManager managerMoip =
		manostuart.server.moip.impl.ComponentFactory.createInstance();
		
	
		// Connect Interfaces
		
		manostuart.server.sistema.spec.req.IMoip moip_sistema_IMoip = 
			(manostuart.server.sistema.spec.req.IMoip)
			managerMoip_Sistema.getProvidedInterface("IMoip");
			managerSistema.setRequiredInterface("IMoip",moip_sistema_IMoip);
		
		manostuart.server.sistema.spec.req.ICadastroProduto cadastrodeproduto_sistema_ICadastroProduto = 
			(manostuart.server.sistema.spec.req.ICadastroProduto)
			managerCadastroDeProduto_Sistema.getProvidedInterface("ICadastroProduto");
			managerSistema.setRequiredInterface("ICadastroProduto",cadastrodeproduto_sistema_ICadastroProduto);
		
		manostuart.server.sistema.spec.req.IGerenteUsuario cadastrodeusuarios_sistema_ICadastroUsuario = 
			(manostuart.server.sistema.spec.req.IGerenteUsuario)
			managerCadastroDeUsuarios_Sistema.getProvidedInterface("ICadastroUsuario");
			managerSistema.setRequiredInterface("ICadastroUsuario",cadastrodeusuarios_sistema_ICadastroUsuario);
		
		manostuart.server.sistema.spec.req.ICadastroCompra cadastrodecompras_sistema_ICadastroCompra = 
			(manostuart.server.sistema.spec.req.ICadastroCompra)
			managerCadastroDeCompras_Sistema.getProvidedInterface("ICadastroCompra");
			managerSistema.setRequiredInterface("ICadastroCompra",cadastrodecompras_sistema_ICadastroCompra);
		
		manostuart.server.sistema.spec.req.ICalculadorFrete calculadorfrete_sistema_ICalculadorFrete = 
			(manostuart.server.sistema.spec.req.ICalculadorFrete)
			managerCalculadorFrete_Sistema.getProvidedInterface("ICalculadorFrete");
			managerSistema.setRequiredInterface("ICalculadorFrete",calculadorfrete_sistema_ICalculadorFrete);
		
		manostuart.server.sistema.spec.req.ICarrinho carrinho_sistema_ICarrinho = 
			(manostuart.server.sistema.spec.req.ICarrinho)
			managerCarrinho_Sistema.getProvidedInterface("ICarrinho");
			managerSistema.setRequiredInterface("ICarrinho",carrinho_sistema_ICarrinho);
		
		manostuart.server.moip.spec.prov.IMoip moip_IMoip = 
			(manostuart.server.moip.spec.prov.IMoip)
			managerMoip.getProvidedInterface("IMoip");
			managerMoip_Sistema.setRequiredInterface("IMoip",moip_IMoip);
		
		manostuart.server.cadastrodeproduto.spec.prov.ICadastroProduto cadastrodeproduto_ICadastroProduto = 
			(manostuart.server.cadastrodeproduto.spec.prov.ICadastroProduto)
			managerCadastroDeProduto.getProvidedInterface("ICadastroProduto");
			managerCadastroDeProduto_Sistema.setRequiredInterface("ICadastroProduto",cadastrodeproduto_ICadastroProduto);
		
//		manostuart.server.gerentedeusuarios.spec.prov.IGerenteUsuario cadastrodeusuarios_ICadastroUsuario = 
//				   (manostuart.server.gerentedeusuarios.spec.prov.IGerenteUsuario)
//				   managerCadastroDeUsuarios.getProvidedInterface("ICadastroUsuario");
//				   managerCadastroDeUsuarios_Sistema.setRequiredInterface("ICadastroUsuario",cadastrodeusuarios_ICadastroUsuario);
		
		manostuart.server.cadastrodecompras.spec.prov.ICadastroCompra cadastrodecompras_ICadastroCompra = 
			(manostuart.server.cadastrodecompras.spec.prov.ICadastroCompra)
			managerCadastroDeCompras.getProvidedInterface("ICadastroCompra");
			managerCadastroDeCompras_Sistema.setRequiredInterface("ICadastroCompra",cadastrodecompras_ICadastroCompra);
		
		manostuart.server.calculadorfrete_sistema.spec.req.ICorreio correio_calculadorfrete_ICorreio = 
			(manostuart.server.calculadorfrete_sistema.spec.req.ICorreio)
			managerCorreio_CalculadorFrete.getProvidedInterface("ICorreio");
			managerCalculadorFrete_Sistema.setRequiredInterface("ICorreio",correio_calculadorfrete_ICorreio);
		
		manostuart.server.correio.spec.prov.ICorreio correio_ICorreio = 
			(manostuart.server.correio.spec.prov.ICorreio)
			managerCorreio.getProvidedInterface("ICorreio");
			managerCorreio_CalculadorFrete.setRequiredInterface("ICorreio",correio_ICorreio);
		
		manostuart.server.carrinho.spec.prov.ICarrinho carrinho_ICarrinho = 
			(manostuart.server.carrinho.spec.prov.ICarrinho)
			managerCarrinho.getProvidedInterface("ICarrinho");
			managerCarrinho_Sistema.setRequiredInterface("ICarrinho",carrinho_ICarrinho);
		
		
		// Set required maps
		
	}

	public String[] getProvidedInterfaces(){
	   List<String> provInterfaceList = new ArrayList<String>();
	   
	   return convertListToArray(provInterfaceList);
	}
	
	public String[] getRequiredInterfaces(){
	
		return convertListToArray(reqInterfaceMap.keySet());
	}
	
	public Object getProvidedInterface(String name){

	   return null;
	}
	
	public void setRequiredInterface(String name, Object facade){
		reqInterfaceMap.put(name,facade);
	}
	
	public Object getRequiredInterface(String name){
	   return reqInterfaceMap.get(name);
	}
	
	private String[] convertListToArray(Collection<String> stringCollection){
		String[] stringArray = new String[stringCollection.size()];
		int i=0;
		for (Iterator<String> iter=stringCollection.iterator();iter.hasNext();){
			stringArray[i] = (String)iter.next();
			i++;
		}
		return stringArray;
	}
}