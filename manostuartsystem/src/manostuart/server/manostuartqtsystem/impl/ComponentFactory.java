package manostuart.server.manostuartqtsystem.impl;

import manostuart.server.manostuartqtsystem.spec.prov.IManager;

public class ComponentFactory {

	private static IManager manager = null;

	public static IManager createInstance(){
	
		if (manager==null)
			manager = new Manager();
		
		return manager;
	}
}



