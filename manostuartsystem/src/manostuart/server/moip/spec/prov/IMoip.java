package manostuart.server.moip.spec.prov;
import manostuart.shared.data.Usuario;


public interface IMoip{

	public boolean efetuarPagamentoBoleto ( int valor, Usuario usr ); 
	public boolean efetuarPagamentoCartao(String cartao);
}