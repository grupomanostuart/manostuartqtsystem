package manostuart.server.gerentedeusuarios_sistema.impl;

import manostuart.server.bancodedados.Bd;

import java.sql.ResultSet;
import java.sql.SQLException;

import manostuart.server.sistema.spec.req.IGerenteUsuario;
import manostuart.shared.data.Usuario;

public class GerenteUsuarioAdapter implements IGerenteUsuario{

	public boolean inserirUsuario ( Usuario usr ){
		
		String cmd;

		//not sure about usr.getnascimento()
		cmd = "INSERT INTO Usuario (login, Senha, Nome, CPF, RG, Nascimento, Sexo) VALUES  ('" +
			  usr.getlogin() + "', '" + usr.getsenha() + "', '" + usr.getnome() + "', '" + usr.getCPF() + "', '" + usr.getRG() +
			  "', '" + usr.getnascimento() + "', '" + usr.getsexo() + "');";

		return Bd.update(cmd);
	}

	public Usuario getUsuario ( String login ){

		String cmd;
		ResultSet rs;
		Usuario u = new Usuario();

		cmd = "select * FROM Usuario WHERE Usuario.Login = '" + login + "';";

		rs = Bd.query(cmd);

		try{
			if(rs.next()){
				u.setlogin(rs.getString(1));
				u.setsenha(rs.getString(2));
				u.setnome(rs.getString(3));
				u.setCPF(rs.getString(4));
				u.setRG(rs.getString(5));
				u.setnascimento(rs.getDate(6));
				u.setsexo(rs.getString(7));
				u.setadmin(rs.getBoolean(8));
			}
		}
		catch(SQLException ex){
			ex.printStackTrace();
		}

		return u;
	}

	public boolean updateUsuario ( Usuario oldUsr, Usuario newUsr ){
		String cmd;

		cmd = "Update Usuario SET Login='" + newUsr.getlogin() + "', Senha ='" + newUsr.getsenha() +
		"', Nome='" + newUsr.getnome() + "', CPF='" + newUsr.getCPF() + "', RG='" + newUsr.getRG() +
		"', Nascimento='" + newUsr.getnascimento() + "', Sexo='" + newUsr.getsexo() + "' WHERE Login='" +
		oldUsr.getlogin() + "';";
		
		System.out.println(cmd);

		return Bd.update(cmd);
	}

	@Override
	public String getEndereco(String login) {
		// TODO Auto-generated method stub
		return null;
	}

}