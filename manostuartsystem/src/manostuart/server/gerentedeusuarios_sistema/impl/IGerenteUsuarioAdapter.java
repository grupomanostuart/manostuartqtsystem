package manostuart.server.gerentedeusuarios_sistema.impl;

import manostuart.server.bancodedados.Bd;
import java.sql.ResultSet;
import java.sql.SQLException;
import manostuart.server.sistema.spec.req.IGerenteUsuario;
import manostuart.shared.data.Usuario;

public class IGerenteUsuarioAdapter implements IGerenteUsuario{

	public boolean inserirUsuario ( Usuario usr ){
		
		String cmd;
		int admin;
		
		if(usr.isadmin())
			admin = 1;
		else
			admin = 0;

		//not sure about usr.getnascimento()
		cmd = "INSERT INTO Usuario (login, Senha, Nome, CPF, RG, Nascimento, Sexo) VALUES  ('" +
			  usr.getlogin() + "', '" + usr.getsenha() + "', '" + usr.getnome() + "', '" + usr.getCPF() + "', '" + usr.getRG() +
			  "', '" + usr.getnascimento() + "', '" + usr.getsexo() + "', " + admin + ");";

		return Bd.update(cmd);
	}

	public Usuario getUsuario ( String login ){

		String cmd;
		ResultSet rs;
		Usuario u = new Usuario();

		cmd = "select * FROM Usuario WHERE Usuario.Login = '" + login + "';";

		rs = Bd.query(cmd);

		try{
			if(rs.next()){
				u.setlogin(rs.getString(1));
				u.setsenha(rs.getString(2));
				u.setnome(rs.getString(3));
				u.setCPF(rs.getString(4));
				u.setRG(rs.getString(5));
				u.setnascimento(rs.getDate(6));
				u.setsexo(rs.getString(7));
				if(rs.getInt(8) == 1)
					u.setadmin(true);
				else
					u.setadmin(false);
			}
		}
		catch(SQLException ex){
			ex.printStackTrace();
		}

		return u;
	}

	public boolean updateUsuario ( Usuario oldUsr, Usuario newUsr ){
		
		String cmd;
		int admin;
		
		if(newUsr.isadmin())
			admin = 1;
		else
			admin = 0;

		cmd = "Update Usuario SET Login='" + newUsr.getlogin() + "', Senha ='" + newUsr.getsenha() +
		"', Nome='" + newUsr.getnome() + "', CPF='" + newUsr.getCPF() + "', RG='" + newUsr.getRG() +
		"', Nascimento='" + newUsr.getnascimento() + "', Sexo='" + newUsr.getsexo() + "', " + admin + " WHERE Login='" +
		oldUsr.getlogin() + "';";

		return Bd.update(cmd);
	}

	public String getEndereco(String login) {
		
		String cmd, end;
		ResultSet rs;
		end = "";
		cmd = "select * from Endereco WHERE Login='" + login + "';";
		
		rs = Bd.query(cmd);
		
		try {
			rs.next();
			end = "Rua: " + rs.getString(3) + "\nBairro: " + rs.getString(6) + "\nNumero: " +
			rs.getInt(4) + "\nComplemento: " + rs.getString(5) + "\nCEP: " + rs.getString(2) +
			"\nCidade: " + rs.getString(8) + "\nEstado: " + rs.getString(7);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return end;
	}

}