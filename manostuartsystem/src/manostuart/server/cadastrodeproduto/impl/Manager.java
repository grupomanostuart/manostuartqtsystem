package manostuart.server.cadastrodeproduto.impl;

import java.util.*;

import manostuart.server.cadastrodeproduto.spec.prov.IManager;


class Manager implements IManager{

	Map<String, Object> reqInterfaceMap = new HashMap<String, Object>();

	public String[] getProvidedInterfaces(){
	   List<String> provInterfaceList = new ArrayList<String>();
	   provInterfaceList.add("ICadastroProduto");
	     
	   
	   return convertListToArray(provInterfaceList);
	}
	
	public String[] getRequiredInterfaces(){
	
		return convertListToArray(reqInterfaceMap.keySet());
	}
	
	public Object getProvidedInterface(String name){

	   if (name.equals("ICadastroProduto")){
	   		return new ICadastroProdutoFacade();
	   }
	   
	   return null;
	}
	
	public void setRequiredInterface(String name, Object facade){
		reqInterfaceMap.put(name,facade);
	}
	
	public Object getRequiredInterface(String name){
	   return reqInterfaceMap.get(name);
	}
	
	private String[] convertListToArray(Collection<String> stringCollection){
		String[] stringArray = new String[stringCollection.size()];
		int i=0;
		for (Iterator<String> iter=stringCollection.iterator();iter.hasNext();){
			stringArray[i] = (String)iter.next();
			i++;
		}
		return stringArray;
	}
}



