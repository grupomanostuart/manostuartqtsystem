package manostuart.server.cadastrodeproduto.spec.prov;
import manostuart.shared.data.Produto;


public interface ICadastroProduto{

	public boolean cadastroProduto ( Produto prod ); 
	public boolean alterarProduto(Produto oldp, Produto newp);

}