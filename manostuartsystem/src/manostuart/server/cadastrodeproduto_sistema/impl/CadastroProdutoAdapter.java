package manostuart.server.cadastrodeproduto_sistema.impl;

import manostuart.server.sistema.spec.req.ICadastroProduto;
import manostuart.server.bancodedados.Bd;
import manostuart.shared.data.Produto;


public class CadastroProdutoAdapter implements ICadastroProduto{

	public boolean cadastroProduto ( Produto prod ){
		
		String cmd;

		cmd = "INSERT INTO Produto (Nome, Marca, Categoria, Foto, Estoque, Preco, Peso, Dim_x, Dim_y, Dim_z) VALUES  ('" +
			  prod.getnome() + "', '" + prod.getmarca() + "', '" + prod.getcategoria() + "', '" + prod.getURLfoto() + "', " + Integer.toString(prod.getestoque()) +
			  ", " + Integer.toString(prod.getpreco()) + ", " + Integer.toString(prod.getpeso()) + ", " + Integer.toString(prod.getdim_x()) + ", " + Integer.toString(prod.getdim_y()) + ", " + Integer.toString(prod.getdim_z()) + ");";

		return Bd.update(cmd);
		
	}
	
	public boolean alterarProduto(Produto oldp, Produto newp){
		
		String cmd;

		cmd = "UPDATE Produto SET Nome='" + newp.getnome() + "', Marca='" + newp.getmarca() +
			  "', Categoria='" + newp.getcategoria() + "', Foto='" + newp.getURLfoto() + 
			  "', Estoque=" + Integer.toString(newp.getestoque()) + ", Preco=" + Integer.toString(newp.getpreco()) +
			  ", Peso=" + Integer.toString(newp.getpeso()) + ", Dim_x=" + Integer.toString(newp.getdim_x()) +
			  ", Dim_y=" + Integer.toString(newp.getdim_y()) + ", Dim_z=" + Integer.toString(newp.getdim_z()) +
			  " WHERE Nome='" + oldp.getnome() + "' AND Marca='" + oldp.getmarca() + "';";

		return Bd.update(cmd);
		
	}
	
	public boolean buscarProduto(Produto prod)
	{
		// Verificar se este m�todo n�o ser� chamado
		
		return false;
	}
}