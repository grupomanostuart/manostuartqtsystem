package manostuart.server.cadastrodeproduto_sistema.impl;

import manostuart.server.cadastrodeproduto_sistema.impl.IManager;

public class ComponentFactory {

	private static IManager manager = null;

	public static IManager createInstance(){
	
		if (manager==null)
			manager = new Manager();
		
		return manager;
	}
}



