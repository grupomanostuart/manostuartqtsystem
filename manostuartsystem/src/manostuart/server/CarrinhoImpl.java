package manostuart.server;

import java.util.ArrayList;

import manostuart.client.CarrinhoService;
import manostuart.server.bancodedados.Bd;
import manostuart.shared.data.*;
import manostuart.server.carrinho_sistema.impl.*;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import javax.servlet.http.HttpSession;

@SuppressWarnings("serial") //no java, u dont get your ID serials
public class CarrinhoImpl extends RemoteServiceServlet implements CarrinhoService 
{
	@SuppressWarnings("unchecked")
	public ArrayList<Produto> GetCarrinho()
	{
		ArrayList<Produto> listaProdutos = null;

		HttpSession session = this.getThreadLocalRequest().getSession();
		if (session.getAttribute("logged") == null || (Boolean) session.getAttribute("logged") == false)
		{
			listaProdutos = (ArrayList<Produto>) session.getAttribute("Carrinho");
		}
		else
		{
			Bd.start_bd();
			ICarrinhoAdapter car = new ICarrinhoAdapter();
			listaProdutos = car.getCarrinho((String)session.getAttribute("login"));
		}
		
		return listaProdutos;
	}

	@SuppressWarnings("unchecked")
	public Boolean AddCarrinho (Produto prod, int num)
	{
		ArrayList<Produto> listaProdutos;
		
		HttpSession session = this.getThreadLocalRequest().getSession();
		if (session.getAttribute("logged") == null || (Boolean) session.getAttribute("logged") == false)
		{
			listaProdutos = (ArrayList<Produto>) session.getAttribute("Carrinho");
			if (listaProdutos == null)
			{
				listaProdutos = new ArrayList<Produto> ();
				session.setAttribute("Carrinho", listaProdutos);
			}
			prod.setqnt(num);
			listaProdutos.add(prod);
		}
		else
		{
			Bd.start_bd();
			ICarrinhoAdapter car = new ICarrinhoAdapter();
			car.adicionarProduto(prod, (String)session.getAttribute("login"), num);
		}
		
		return true;
	}
	
	public ArrayList<Produto> RefreshCarrinho(ArrayList<Produto> list)
	{
		HttpSession session = this.getThreadLocalRequest().getSession();
		if (session.getAttribute("logged") == null || (Boolean) session.getAttribute("logged") == false)
		{
			session.setAttribute("Carrinho", list);
		}
		else
		{
			Bd.start_bd();
			ICarrinhoAdapter car = new ICarrinhoAdapter();
			car.dropCarrinho((String)session.getAttribute("login"));
			car.addListaCarrinho((String)session.getAttribute("login"), list);
		}
		
		return list;
	}
}