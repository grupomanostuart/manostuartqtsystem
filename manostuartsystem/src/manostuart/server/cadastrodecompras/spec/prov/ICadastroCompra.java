package manostuart.server.cadastrodecompras.spec.prov;
import manostuart.shared.data.*;

public interface ICadastroCompra{

	public boolean cadastrarCompra ( Compra compra ); 
	public Compra getCompra(int id);
	public boolean setPago(int id, boolean state);
	public boolean gerarComprovante ( String com_id ); 
}