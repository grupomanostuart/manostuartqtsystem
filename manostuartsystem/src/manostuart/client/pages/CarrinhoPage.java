package manostuart.client.pages;

import java.util.ArrayList;

import manostuart.client.CompraService;
import manostuart.client.CompraServiceAsync;
import manostuart.client.ContentContainer;
import manostuart.client.CarrinhoService;
import manostuart.client.CarrinhoServiceAsync;
import manostuart.shared.data.Compra;
import manostuart.shared.data.Produto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;

public class CarrinhoPage extends PageDefs
{

   public ArrayList<Produto> items;
   public CarrinhoPage(ArrayList<Produto> it)
   {
       items = it;
   }
   
   @Override
   public String getToken()
   {
      return "CARRINHO";
   }

   @Override
   public String getWindowTitle()
   {
      return "Meu Carrinho";
   }
   
   @Override
   public Composite getContent()
   {
      return new CarrinhoContent(items);
   }

   @Override
   public Composite getNavigation()
   {
      return new CarrinhoNavigation();
   }
}

class CarrinhoContent extends Composite
{
	public ArrayList<ResultDataCarrinho> results;
   public CarrinhoContent(final ArrayList<Produto> items)
   {
	  int price = 0;
      VerticalPanel vp = new VerticalPanel();
      initWidget(vp);
      if(items != null)
      {
    	  if (items.size() == 0)
    		  vp.add(new HTML("<h1>Seu Carrinho esta vazio</h1>"));
    	  else
    	  {
	         int numRows = items.size();
	         Grid grid = new Grid(numRows, 1);
	         results = new ArrayList<ResultDataCarrinho>();
	         for (int row = 0; row < numRows; row++)
	         {
	        	price += items.get(row).getpreco() * items.get(row).getqnt();
	        	ResultDataCarrinho aux = new ResultDataCarrinho(items.get(row));
	        	results.add(aux);
	            grid.setWidget(row, 0, aux);	            
	         }
	         final int total = price;
	         
	         vp.add(grid);
	         
	         Button refresh = new Button("Refresh", 
	                 new ClickHandler() 
	                 {
		                   public void onClick(ClickEvent event) 
		                   {
								 CarrinhoServiceAsync CarrinhoService = GWT.create(CarrinhoService.class);
								 ArrayList<Produto> list = new ArrayList<Produto>();
								 for (int row = 0; row < items.size(); row++)
								 {
									 int num = Integer.parseInt(results.get(row).qnt.getText());
									 if (num == 0)
										 list.add(items.get(row));
									 else
										 items.get(row).setqnt(num);
								 }
								 for (int row = 0; row < list.size(); row++)
									 items.remove(list.get(row));
								 CarrinhoService.RefreshCarrinho(items, new AsyncCallback<ArrayList<Produto>>() {
									 @Override
									 public void onSuccess(ArrayList<Produto> result) 
									 {
										 ContentContainer.getInstance().setPageByToken(PageDefs.CarrinhoToken, result);
									 }
									 
									 @Override
									 public void onFailure(Throwable caught) 
									 {
									     // TODO Auto-generated method stub
									 }
								 });
		                   }
	                 });	         
	         
	         Button comprar = new Button("Efetuar Compra", 
	                 new ClickHandler() 
	                 {
		                   public void onClick(ClickEvent event) 
		                   {
								 CarrinhoServiceAsync CarrinhoService = GWT.create(CarrinhoService.class);                  
								 CarrinhoService.GetCarrinho(new AsyncCallback<ArrayList<Produto>>() {
									 @Override
									 public void onSuccess(ArrayList<Produto> listaProds) 
									 {										 
										 CompraServiceAsync CompraService = GWT.create(CompraService.class);
										 CompraService.comprar(listaProds, total, new AsyncCallback<Compra>() {											 
											 @Override
											 public void onSuccess(Compra c)
											 {
												 if (c != null)
													 ContentContainer.getInstance().setPageByToken(PageDefs.PagamentoToken, c);
												 else
													 Window.alert("Por favor faca o login antes de efetuar a comprar");												 
											 }
											 @Override
											 public void onFailure(Throwable caught)
											 {
												 
											 }										 
										
											 
										 });
									 }
									 
									 @Override
									 public void onFailure(Throwable caught) 
									 {

									 }
								 });
								 
		                   }
	                 });
	         
	         vp.add(refresh);
	         vp.add(new HTML("</br>Total: R$ " + price/100));
	         vp.add(comprar);
    	  }
      }
      
      else
    	  vp.add(new HTML("<h1>Seu Carrinho esta vazio</h1>"));
   }
}

class CarrinhoNavigation extends DefaultNavigation
{
   public CarrinhoNavigation()
   {
      super();
   }

   

}
