package manostuart.client.pages;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;

public class WelcomePage extends PageDefs
{

    @Override
    public String getToken()
    {
        return "WELCOME";
    }

    @Override
    public String getWindowTitle()
    {
        return "Bem-vindo";
    }
    
    @Override
    public Composite getContent()
    {
       return new WelcomeContent();
    }

    @Override
    public Composite getHeader()
    {
       return new WelcomeHeader();
    }

    @Override
    public Composite getNavigation()
    {
       return new WelcomeNavigation();
    }

}

class WelcomeContent extends Composite
{
   public WelcomeContent()
   {
      VerticalPanel vp = new VerticalPanel();
      initWidget(vp);
      vp.add(new HTML("<div>"
              + "<h1>Bem vindo ao sistema ManoStuart de gerenciamento</h1></br>"
              + "</div>"));
   }
}

class WelcomeHeader extends DefaultHeader
{
   public WelcomeHeader()
   {
      super();
   }
}

class WelcomeNavigation extends DefaultNavigation
{
   public WelcomeNavigation()
   {
       super();
   }
}