package manostuart.client.pages;


import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import manostuart.client.CompraService;
import manostuart.client.CompraServiceAsync;
import manostuart.client.ContentContainer;
import manostuart.shared.data.Compra;



public class PagamentoPage extends PageDefs
{ 
    Compra compra;
    String endereco;
    
    
    public PagamentoPage(Compra c)
    {
        compra = c;
        endereco = c.getendereco();
    }

    @Override
    public String getToken()
    {
        return "Pagamento";
    }

    @Override
    public String getWindowTitle()
    {
        return "Visualizar Pagamento";
    }
    
    @Override
    public Composite getContent()
    {
       return new PagamentoContent(compra, endereco);
    }

    @Override
    public Composite getHeader()
    {
       return new PagamentoHeader();
    }

    @Override
    public Composite getNavigation()
    {
       return new PagamentoNavigation();
    }

}


class PagamentoContent extends Composite
{
   static TextArea endr = new TextArea();   
   static TextBox numCartao = new TextBox();
   
   
   public static String getEndereco()
   {
      return endr.getText();
   }
   
   public static int getNumCartao()
   {
	   return Integer.parseInt(numCartao.getText());
   }
   
   public PagamentoContent(final Compra c, final String endereco)
   {
      
      final VerticalPanel vp = new VerticalPanel();
      initWidget(vp);
      String preco = Integer.toString(c.getvalor()/100) + "," + Integer.toString(c.getvalor()%100);      
      vp.add(new HTML("<div>"
              + "</br>ID: " + c.getid()
              + "</br>Valor: " + preco
              + "</br>Data: " + c.getdata()
              + "</br>Endereco: " + c.getendereco()
              + "</br></div>"));
      vp.add(endr);
      endr.setText(endereco);
      endr.setVisible(false);
      endr.setVisibleLines(7);
      Button alterarEnd = new Button("Alterar Endereco",
    		  new ClickHandler() 
		      {
    	  		public void onClick(ClickEvent event)
    	  		{    	  			
    	  			endr.setVisible(true);
    	  		}		      
		      });
      
      
      vp.add(alterarEnd);      
      vp.add(new HTML("<div><br>Numero do Cartao: </div>"));
      vp.add(numCartao);
      Button pagar = new Button("Pagar",
         new ClickHandler() 
         {
           public void onClick(ClickEvent event)
           {
              int numCartao = PagamentoContent.getNumCartao();
              
              if(numCartao % 2 == 0)
              {
                  // pagamento deu certo
            	  CompraServiceAsync CompraService = GWT.create(CompraService.class);
            	  CompraService.setPagamentoEfetuado(endr.getText(), new AsyncCallback<Boolean>() {
					
					@Override
					public void onSuccess(Boolean result) {
				         Window.alert("Compra Faturada!");		
				         CompraServiceAsync CompraService = GWT.create(CompraService.class);
	                	 CompraService.getComprasHistorico(new AsyncCallback<ArrayList<Compra>>() {							
							@Override
							public void onSuccess(ArrayList<Compra> result) {
								ContentContainer.getInstance().setPageByToken(PageDefs.CompraToken, result);								
							}							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub								
							}
						 });
				         return;						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});            	  
              }                 
              else
              {           	  
              	  // sem pagamento
            	  Window.alert("Numero de cartao invalido!");
              }                 
           }
         });
      vp.add(pagar);

    	  
   }
}



class PagamentoHeader extends DefaultHeader
{
   public PagamentoHeader()
   {
      super();
      hp.add(new HTML("<h1>Voce esta fazendo um Pagamento </h1>"));
   }
}

class PagamentoNavigation extends DefaultNavigation
{
   public PagamentoNavigation()
   {
      super();
   }
}