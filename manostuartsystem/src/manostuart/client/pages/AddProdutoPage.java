package manostuart.client.pages;

import manostuart.client.CadastroProdutoService;
import manostuart.client.CadastroProdutoServiceAsync;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class AddProdutoPage extends PageDefs
{

    @Override
    public String getToken()
    {
        return "ADDPRODUTO";
    }

    @Override
    public String getWindowTitle()
    {
        return "Adicionar Produto";
    }
    
    @Override
    public Composite getContent()
    {
       return new AddProdutoContent();
    }

    @Override
    public Composite getNavigation()
    {
       return new AddProdutoNavigation();
    }

}


class AddProdutoContent extends Composite
{
   public AddProdutoContent()
   {
      VerticalPanel vp = new VerticalPanel();
      initWidget(vp);
      final TextBox t0 = new TextBox();
      t0.setText("Nome");
      final TextBox t1 = new TextBox();
      t1.setText("Marca");
      final TextBox t2 = new TextBox();
      t2.setText("Categoria");
      final TextBox t3 = new TextBox();
      t3.setText("Preco");
      //final TextBox t4 = new TextBox();
      //t4.setText("URL Foto");
      final TextBox t5 = new TextBox();
      t5.setText("Estoque");
      final TextBox t6 = new TextBox();
      t6.setText("Peso");
      final TextBox t7 = new TextBox();
      t7.setText("Largura");
      final TextBox t8 = new TextBox();
      t8.setText("Comprimento");
      final TextBox t9 = new TextBox();
      t9.setText("Altura");

      vp.add(t0);
      vp.add(t1);
      vp.add(t2);
      vp.add(t3);
      //vp.add(t4);
      vp.add(t5);
      vp.add(t6);
      vp.add(t7);
      vp.add(t8);
      vp.add(t9);
      
      Button send = new Button( "Adicionar Produto", 
            new ClickHandler() 
            {
              public void onClick(ClickEvent event) 
              {
                 CadastroProdutoServiceAsync cadService = GWT.create(CadastroProdutoService.class);

                 cadService.CadastroProduto(t0.getText(), t1.getText(), t2.getText(), "./pics/manostuart.jpg",
                       Integer.parseInt(t5.getText()), Integer.parseInt(t3.getText()), 
                       Integer.parseInt(t6.getText()), Integer.parseInt(t7.getText()), 
                       Integer.parseInt(t8.getText()), Integer.parseInt(t9.getText()), new AsyncCallback<Boolean>()
                          {
                          
                          @Override
                          public void onSuccess(Boolean result) 
                          {
                             if(result)
                                Window.alert("Produto adicionado com sucesso");
                             else
                                Window.alert("Houve um erro ao adicionar produto");
                            
                          }
                          
                          @Override
                          public void onFailure(Throwable caught) 
                          {
                              // TODO Auto-generated method stub
                              
                          }
                      });
              }
            });
      vp.add(send);
      
   }
}

class AddProdutoNavigation extends DefaultNavigation
{
   public AddProdutoNavigation()
   {
       super();
   }
}