package manostuart.client.pages;

import manostuart.client.CompraService;
import manostuart.client.CompraServiceAsync;
import manostuart.client.ContentContainer;
import manostuart.client.CarrinhoService;
import manostuart.client.CarrinhoServiceAsync;
import manostuart.client.LoginUsuarioService;
import manostuart.client.LoginUsuarioServiceAsync;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import manostuart.shared.data.Compra;
import manostuart.shared.data.Produto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public abstract class PageDefs
{
   public static final String WelcomeToken = "WELCOME";
   public static final String WelcomeTitle = "Bem-vindo";
   public static final String ProdutoToken = "PRODUTO";
   public static final String Produto = "Visualizar Produto";
   public static final String BuscaToken = "BUSCA";
   public static final String BuscaTitle = "Fazer Busca";
   public static final String LoginToken = "LOGIN";
   public static final String LoginTitle = "Sign Up";
   public static final String AddProdutoToken = "ADDPRODUTO";
   public static final String AddProdutoTitle = "Adicionar Produto";
   public static final String ResultToken = "RESULT";
   public static final String ResultTitle = "Resultados da busca";
   public static final String CarrinhoToken = "CARRINHO";
   public static final String CarrinhoTitle = "Meu Carrinho";
   public static final String CompraToken = "COMPRA";
   public static final String CompraTitle = "Minhas Compras";
   public static final String PagamentoToken = "Pagamento";
   public static final String PagamentoTitle = "Visualizar Pagamento";
   
   public abstract String getToken();  
   public abstract String getWindowTitle();
   
   
   public Composite getContent()
   {
      return new DefaultContent();
   }

   public Composite getHeader()
   {
      return new DefaultHeader();
   }

   public Composite getNavigation()
   {
      return new DefaultNavigation();
   }
}


class DefaultContent extends Composite
{
   public VerticalPanel vp;
   public DefaultContent()
   {
      vp = new VerticalPanel();
      initWidget(vp);
      vp.add(new HTML("</br>"));
   }
}

class DefaultHeader extends Composite
{
   public HorizontalPanel hp;
   public DefaultHeader()
   {
      hp = new HorizontalPanel();
      initWidget(hp);
      hp.setSpacing(10);
       
      Button n0 = new Button( "Home", 
              new ClickHandler() 
              {
                public void onClick(ClickEvent event) 
                {
                    ContentContainer.getInstance().setPageByToken(PageDefs.WelcomeToken);
                }
              });
        hp.add(n0);

	    final LoginUsuarioServiceAsync checks = GWT.create(LoginUsuarioService.class);                  
	    checks.CheckLogin(new AsyncCallback<Boolean>() {
	        @Override
	        public void onSuccess(Boolean result) 
	        {
	      	  if (!result)
	      	  {
	      		  Button n1 = new Button( "Login", 
	      		            new ClickHandler() 
	      		            {
	      		              public void onClick(ClickEvent event) 
	      		              {
	      		                  ContentContainer.getInstance().setPageByToken(PageDefs.LoginToken);
	      		              }
	      		            });
	      		      hp.add(n1);
	      	  }
	      	  else
	      	  {
		      		Button n1 = new Button( "Logout", 
	      		            new ClickHandler() 
	      		            {
		      		              public void onClick(ClickEvent event) 
		      		              {
		      		            	checks.Logout(new AsyncCallback<Boolean>() {
			      		      	        @Override
			      		      	        public void onSuccess(Boolean result) 
			      		      	        {
				      		                  ContentContainer.getInstance().setPageByToken(PageDefs.WelcomeToken);
			      		      	        }
		
				      		  	        @Override
				      		  	        public void onFailure(Throwable caught) 
				      		  	        {
				      		  	            // TODO Auto-generated method stub
				                            Window.alert("Ocorreu algum erro.");
				      		  	        }
		      		                });
		      		              }
	      		            });
	      		      hp.add(n1);
	      	  }
	        }
	        
	        @Override
	        public void onFailure(Throwable caught) 
	        {
	            // TODO Auto-generated method stub
	        }
	    });

      Button n3 = new Button( "Busca", 
              new ClickHandler() 
              {
                public void onClick(ClickEvent event) 
                {
                    ContentContainer.getInstance().setPageByToken(PageDefs.BuscaToken);
                }
              });
        hp.add(n3);
                      
        checks.CheckManager(new AsyncCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean result) 
            {
          	  if (result)
          	  {
	          		Button n4 = new Button( "AddProduto", 
	                        new ClickHandler() 
	                        {
	                          public void onClick(ClickEvent event) 
	                          {
	                              ContentContainer.getInstance().setPageByToken(PageDefs.AddProdutoToken);
	                          }
	                        });
	                  hp.add(n4);
          	  }
            }
            
            @Override
            public void onFailure(Throwable caught) 
            {
                // TODO Auto-generated method stub
            }
        });
      
          Button n5 = new Button( "Carrinho", 
                new ClickHandler() 
                {
                  public void onClick(ClickEvent event) 
                  {
                	  CarrinhoServiceAsync CarrinhoService = GWT.create(CarrinhoService.class);                  
                	  CarrinhoService.GetCarrinho(new AsyncCallback<ArrayList<Produto>>() {
                          @Override
                          public void onSuccess(ArrayList<Produto> result) 
                          {
                        	  ContentContainer.getInstance().setPageByToken(PageDefs.CarrinhoToken, result);
                          }
                          
                          @Override
                          public void onFailure(Throwable caught) 
                          {
                              // TODO Auto-generated method stub
                          }
                	  });
                  }
                });
          hp.add(n5);
          
          Button n6 = new Button( "Compras", 
                new ClickHandler() 
                {
                  public void onClick(ClickEvent event) 
                  {                	  
                	  CompraServiceAsync CompraService = GWT.create(CompraService.class);
                	  CompraService.getComprasHistorico(new AsyncCallback<ArrayList<Compra>>() {
						
						@Override
						public void onSuccess(ArrayList<Compra> result) {
							ContentContainer.getInstance().setPageByToken(PageDefs.CompraToken, result);
							
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
					});
                      
                  }
                });
          hp.add(n6);
   }
}

class DefaultNavigation extends Composite
{
   public VerticalPanel vp;
   public DefaultNavigation()
   {
      vp = new VerticalPanel();
      initWidget(vp);
      vp.add(new HTML("</br>"));
   }
}
