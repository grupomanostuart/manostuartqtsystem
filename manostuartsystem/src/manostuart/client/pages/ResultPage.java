package manostuart.client.pages;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;

import manostuart.client.ContentContainer;
import manostuart.shared.data.Produto;

public class ResultPage extends PageDefs
{
    public ArrayList<Produto> results;
    public ResultPage(ArrayList<Produto> res)
    {
        results = res;
    }

    @Override
    public String getToken()
    {
        return "RESULT";
    }

    @Override
    public String getWindowTitle()
    {
        return "Resultados da busca";
    }
    
    @Override
    public Composite getContent()
    {
       return new ResultContent(results);
    }

    @Override
    public Composite getNavigation()
    {
       return new ResultNavigation();
    }

}


class ResultContent extends Composite
{
   public ResultContent(ArrayList<Produto> res)
   {	  
      VerticalPanel vp = new VerticalPanel();
      initWidget(vp);
      int numRows = res.size();
      Grid grid = new Grid(numRows, 1);
      for (int row = 0; row < numRows; row++)
          grid.setWidget(row, 0, new ResultData(res.get(row)));
      vp.add(grid);
   }
}

class ResultNavigation extends DefaultNavigation
{
   public ResultNavigation()
   {
	   super();
   }
}

class ResultData extends Composite
{
    public ResultData(final Produto p)
    {
        HorizontalPanel hp = new HorizontalPanel();
        initWidget(hp);
        Image i = new Image(p.getURLfoto());
        i.setPixelSize(120, 120);
        PushButton normalPushButton = new PushButton(i,
                new ClickHandler() 
        {
          public void onClick(ClickEvent event) 
          {
              ContentContainer.getInstance().setPageByToken(PageDefs.ProdutoToken, p);
          }
        });
        hp.add(normalPushButton);
        TextBox nameText = new TextBox();
        nameText.setText(p.getnome());
        nameText.setEnabled(false);
        TextBox valorText = new TextBox();
        String str = Integer.toString(p.getpreco()/100) + "," + Integer.toString(p.getpreco()%100);
        valorText.setText(str);
        valorText.setEnabled(false);
        hp.add(nameText);
        hp.add(valorText);        
    }
}

class ResultDataCarrinho extends Composite
{
	public TextBox qnt;
	public ResultDataCarrinho (final Produto p)
	{
		HorizontalPanel hp = new HorizontalPanel();
        initWidget(hp);
        Image i = new Image(p.getURLfoto());
        i.setPixelSize(120, 120);
        PushButton normalPushButton = new PushButton(i,
                new ClickHandler() 
        {
          public void onClick(ClickEvent event) 
          {
              ContentContainer.getInstance().setPageByToken(PageDefs.ProdutoToken, p);
          }
        });
        hp.add(normalPushButton);
        TextBox nameText = new TextBox();
        nameText.setText(p.getnome());
        nameText.setEnabled(false);
        TextBox valorText = new TextBox();
        String str = Integer.toString(p.getpreco()/100) + "," + Integer.toString(p.getpreco()%100);
        valorText.setText(str);
        valorText.setEnabled(false);
        hp.add(nameText);
        hp.add(valorText); 
        
        qnt = new TextBox();
        qnt.setText("" + p.getqnt());
        hp.add(qnt);
	}
}