package manostuart.client.pages;

import java.util.ArrayList;

import manostuart.client.ContentContainer;
import manostuart.shared.data.Compra;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class CompraPage extends PageDefs
{

   public ArrayList<Compra> compras;
   public CompraPage(ArrayList<Compra> it)
   {
       compras = it;
   }
   
   @Override
   public String getToken()
   {
      return "Compra";
   }

   @Override
   public String getWindowTitle()
   {
      return "Minhas Compras";
   }
   
   @Override
   public Composite getContent()
   {
      return new CompraContent(compras);
   }

   @Override
   public Composite getNavigation()
   {
      return new CompraNavigation();
   }
}

class CompraContent extends Composite
{
   public CompraContent(ArrayList<Compra> items)
   {
      VerticalPanel vp = new VerticalPanel();
      initWidget(vp);
      if(items != null)
      {
         int numRows = items.size();
         Grid grid = new Grid(numRows, 1);
         for (int row = 0; row < numRows; row++)
            grid.setWidget(row, 0, new CompraData(items.get(row)));
         vp.add(grid);
      }
      else
      {
         vp.add(new HTML("<h1>Voce nao possui nenhuma compra.</h1>"));
      }

   }
}

class CompraNavigation extends DefaultNavigation
{
   public CompraNavigation()
   {
      super();
   }
}


class CompraData extends Composite
{
    public CompraData(final Compra p)
    {
        HorizontalPanel hp = new HorizontalPanel();
        initWidget(hp);
        
        /*
        PushButton normalPushButton = new PushButton(String.valueOf(p.getid()), new ClickHandler() 
        {
          public void onClick(ClickEvent event) 
          {
              ContentContainer.getInstance().setPageByToken(PageDefs.ProdutoToken, p);
          }
        });
        
        hp.add(normalPushButton);*/
        TextBox nameText = new TextBox();
        nameText.setText(p.getdata());
        nameText.setEnabled(false);        
        nameText.setVisibleLength(30);
        TextBox valorText = new TextBox();        
        String str = "R$ " + Integer.toString(p.getvalor()/100) + "," + Integer.toString(p.getvalor()%100);
        valorText.setText(str);
        valorText.setEnabled(false);
        TextBox pagoText = new TextBox();
        if (p.getpago())
        	pagoText.setText("Pago");
        else
        	pagoText.setText("Nao Pago");
        pagoText.setEnabled(false);
        hp.add(nameText);
        hp.add(valorText);
        hp.add(pagoText);
    }
}