package manostuart.client.pages;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import manostuart.shared.data.Produto;
import manostuart.client.CarrinhoService;
import manostuart.client.CarrinhoServiceAsync;
import manostuart.client.ContentContainer;



public class ProdutoPage extends PageDefs
{
    Produto produto;
    
    public ProdutoPage(Produto p)
    {
        produto = p;
    }

    @Override
    public String getToken()
    {
        return "PRODUTO";
    }

    @Override
    public String getWindowTitle()
    {
        return "Visualizar Produto";
    }
    
    @Override
    public Composite getContent()
    {
       return new ProdutoContent(produto);
    }
    
    @Override
    public Composite getNavigation()
    {
       return new ProdutoNavigation();
    }

}


class ProdutoContent extends Composite
{
   final TextBox qnt = new TextBox();
   public ProdutoContent(final Produto p)
   {
      VerticalPanel vp = new VerticalPanel();
      initWidget(vp);
      String preco = Integer.toString(p.getpreco()/100) + "," + Integer.toString(p.getpreco()%100);      
      vp.add(new HTML("<div>"
    		  + "<img src=\"" + p.getURLfoto() + "\" width=\"120\" height=\"auto\"></img>"
              + "</br>Nome: " + p.getnome()
              + "</br>Valor: " + preco
              + "</br>Marca: " + p.getmarca()
              + "</br>Categoria: " + p.getcategoria()
              + "</br>Estoque: " + p.getestoque()
              + "</br>Dimensoes: " + p.getdim_x() + " x " + p.getdim_y() + " x " + p.getdim_z() 
              + "</br>Peso: " + p.getpeso() + "g"
              + "</br></br></div>"));
      
      qnt.setText("1");
      vp.add(qnt);
      
      Button AddCarrinho = new Button("Adicionar ao Carrinho", 
              new ClickHandler() 
              {
                public void onClick(ClickEvent event) 
                {
                  int num = Integer.parseInt(qnt.getText());
                  CarrinhoServiceAsync CarrinhoService = GWT.create(CarrinhoService.class);                  
                  CarrinhoService.AddCarrinho(p, num, new AsyncCallback<Boolean>() {
                      @Override
                      public void onSuccess(Boolean result) 
                      {
                    	  if(result)
                    	  {
                              Window.alert("Produto adicionado ao Carrinho");
                              ContentContainer.getInstance().setPageByToken(PageDefs.WelcomeToken);
                    	  }
                           else
                              Window.alert("Houve algum erro no sistema");
                      }
                      
                      @Override
                      public void onFailure(Throwable caught) 
                      {
                          // TODO Auto-generated method stub
                      }
                  });
              }
            });
  
      vp.add(AddCarrinho);
   }
}

class ProdutoNavigation extends DefaultNavigation
{
   public ProdutoNavigation()
   {
	   super();
   }
}