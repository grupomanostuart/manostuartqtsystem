package manostuart.client.pages;

import manostuart.client.LoginUsuarioService;
import manostuart.client.LoginUsuarioServiceAsync;
import manostuart.client.ContentContainer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;


public class LoginPage extends PageDefs
{
   
   @Override
   public String getToken()
   {
      return "LOGIN";
   }

   @Override
   public String getWindowTitle()
   {
      return "Sign Up";
   }

   @Override
   public Composite getContent()
   {
      return new LoginContent();
   }

   @Override
   public Composite getNavigation()
   {
      return new LoginNavigation();
   }
}

class LoginContent extends Composite
{
   final TextBox nameField = new TextBox();
   final TextBox passField = new TextBox();
   
   public LoginContent()
   {
      VerticalPanel vp = new VerticalPanel();
      initWidget(vp);

      vp.add(nameField);
      vp.add(passField);
      nameField.setText("Usuario");
      passField.setText("Senha");
      
      Button sendButton = new Button("Log In", 
              new ClickHandler() 
              {
                public void onClick(ClickEvent event) 
                {
                  LoginUsuarioServiceAsync LoginService = GWT.create(LoginUsuarioService.class);                  
                  LoginService.LoginUsuario(nameField.getText(),passField.getText(), new AsyncCallback<Boolean>() {
                      @Override
                      public void onSuccess(Boolean result) 
                      {
                    	  if(result)
                    	  {
                              Window.alert("Bem vindo " + nameField.getText());
                              ContentContainer.getInstance().setPageByToken(PageDefs.WelcomeToken);
                    	  }
                           else
                              Window.alert("Login ou senha incorretos!");
                      }
                      
                      @Override
                      public void onFailure(Throwable caught) 
                      {
                          // TODO Auto-generated method stub
                      }
                  });
              }
            });
  
      vp.add(sendButton);
   }
}

class LoginNavigation extends DefaultNavigation
{
   public LoginNavigation()
   {
      super();
   }
}