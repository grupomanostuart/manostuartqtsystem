package manostuart.client.pages;

import java.util.ArrayList;

import manostuart.client.BuscaProdutoService;
import manostuart.client.BuscaProdutoServiceAsync;
import manostuart.client.ContentContainer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import manostuart.shared.data.Produto;

public class BuscaPage extends PageDefs
{

    @Override
    public String getToken()
    {
        return "BUSCA";
    }

    @Override
    public String getWindowTitle()
    {
        return "Fazer Busca";
    }
    
    @Override
    public Composite getContent()
    {
       return new BuscaContent();
    }

    @Override
    public Composite getNavigation()
    {
       return new BuscaNavigation();
    }

}


class BuscaContent extends Composite
{
   final TextBox searchField = new TextBox();
   public BuscaContent()
   {
      VerticalPanel vp = new VerticalPanel();
      initWidget(vp);
      vp.add(new HTML("<div>"
              + "<h1>Aqui voce faz uma busca</h1>"
              + "</div>"));
      vp.add(searchField);
      Button send = new Button("Buscar", 
              new ClickHandler() 
              {
                public void onClick(ClickEvent event) 
                {
                  BuscaProdutoServiceAsync buscaService = 
                          GWT.create(BuscaProdutoService.class);
                  // Trocar o int 0 pelo filtro escolhido 
                  // 0 == busca por nome, 1 == marca, 2 == categoria
                  int opt = BuscaNavigation.getSearchOption();
                  buscaService.BuscaProduto(searchField.getText(),opt, new AsyncCallback<ArrayList<Produto>>() {
                      
                      @Override
                      public void onSuccess(ArrayList<Produto> result)
                      {
                    	  ContentContainer.getInstance().setPageByToken(PageDefs.ResultToken, result);
                      }
                      
                      @Override
                      public void onFailure(Throwable caught) 
                      {
                          // TODO Auto-generated method stub
                          
                      }
                  });
              }
            });
      vp.add(send);
   }
}

class BuscaNavigation extends Composite
{
   public static RadioButton r0,r1,r2; 
   
   public static int getSearchOption()
   {
      if(r1.getValue())
         return 1;
      else if(r2.getValue())
         return 2;
      else
         return 0;
   }
   
   public BuscaNavigation()
   {
      VerticalPanel vp = new VerticalPanel();
      initWidget(vp);
      vp.add(new HTML("<h1>Filtros</h1>"));
      r0 = new RadioButton("search", "Pesquisa por Nome");
      r1 = new RadioButton("search", "Pesquisa por Marca");
      r2 = new RadioButton("search", "Pesquisa por Categoria");
      r0.setValue(true);
      r1.setValue(false);
      r2.setValue(false);
      vp.add(r0);
      vp.add(r1);
      vp.add(r2);
   }
}