package manostuart.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface LoginUsuarioServiceAsync
{
	void LoginUsuario(String name, String psw, AsyncCallback<Boolean> asyncCallback);
	
	void CheckLogin (AsyncCallback<Boolean> asyncCallback);

	void CheckManager (AsyncCallback<Boolean> asyncCallback);

	void Logout (AsyncCallback<Boolean> asyncCallback);
}