package manostuart.client;

import java.util.ArrayList;

import manostuart.shared.data.Produto;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("buscaproduto")
public interface BuscaProdutoService extends RemoteService
{
	ArrayList<Produto> BuscaProduto(String str, int opcao);   
}
