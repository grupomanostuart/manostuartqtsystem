package manostuart.client;

import java.sql.SQLException;
import java.util.ArrayList;

import manostuart.shared.data.Compra;
import manostuart.shared.data.Produto;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface CompraServiceAsync
{
	void comprar(ArrayList<Produto> prod, int total, AsyncCallback<Compra> asyncCallback);
	void setPagamentoEfetuado(String servico, AsyncCallback<Boolean> asyncCallback);
	void getComprasHistorico(AsyncCallback<ArrayList<Compra>> asyncCallback);

}
