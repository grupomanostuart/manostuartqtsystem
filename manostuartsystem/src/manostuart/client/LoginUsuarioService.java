package manostuart.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("LoginUsuario")
public interface LoginUsuarioService extends RemoteService
{
	Boolean LoginUsuario(String name, String psw);
	
	Boolean CheckLogin ();
	
	Boolean CheckManager ();
	
	Boolean Logout ();
}
