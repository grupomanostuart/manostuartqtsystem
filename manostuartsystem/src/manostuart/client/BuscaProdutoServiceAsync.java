package manostuart.client;

import java.util.ArrayList;
import manostuart.shared.data.Produto;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface BuscaProdutoServiceAsync
{
	void BuscaProduto(String str, int opcao, AsyncCallback<ArrayList<Produto>> asyncCallback);
}