package manostuart.client;

import java.util.ArrayList;

import manostuart.shared.data.Produto;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("Carrinho")
public interface CarrinhoService extends RemoteService
{
	ArrayList<Produto> GetCarrinho();
	
	Boolean AddCarrinho(Produto prod, int num);
	
	ArrayList<Produto> RefreshCarrinho(ArrayList<Produto> list);
}
