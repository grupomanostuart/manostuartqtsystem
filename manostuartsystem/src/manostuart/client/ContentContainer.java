package manostuart.client;

import java.util.ArrayList;

import manostuart.client.pages.*;
import manostuart.shared.data.Compra;
import manostuart.shared.data.Produto;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Composite;

public class ContentContainer extends Composite implements ValueChangeHandler<String>
{   
   
   private static ContentContainer myInstance = new ContentContainer();
   public static synchronized ContentContainer getInstance() 
   {
       return myInstance;
   }
   private ContentContainer() 
   {
       History.addValueChangeHandler(this);
   }
   
   
   public void onValueChange(ValueChangeEvent<String> event)
   {
      String token = (String) event.getValue();
      this.setPageByToken(token);
   }
   
   public  void setPageByToken(String token)
   {
       setPageByToken(token, null);
   }

@SuppressWarnings("unchecked")
public  void setPageByToken(String token, Object o) 
  {
      if (token.equals(PageDefs.WelcomeToken)) 
          this.setPage(new WelcomePage());         
      else if (token.equals(PageDefs.LoginToken))
          this.setPage(new LoginPage());
      else if (token.equals(PageDefs.BuscaToken))
          this.setPage(new BuscaPage());
      else if (token.equals(PageDefs.AddProdutoToken))
          this.setPage(new AddProdutoPage());
      else if (token.equals(PageDefs.ProdutoToken))
          this.setPage(new ProdutoPage((Produto) o)); 
      else if (token.equals(PageDefs.ResultToken))
          this.setPage(new ResultPage((ArrayList<Produto>) o));
      else if (token.equals(PageDefs.CarrinhoToken))
         this.setPage(new CarrinhoPage((ArrayList<Produto>) o));
      else if (token.equals(PageDefs.CompraToken))
         this.setPage(new CompraPage((ArrayList<Compra>) o));      
      else if (token.equals(PageDefs.PagamentoToken))
    	  this.setPage(new PagamentoPage((Compra) o)); 
      else 
          Window.alert("Error in parsing history token: '" + token + "'");
      
  }
  
  
  public void setPage(PageDefs page)
  {
     History.newItem(page.getToken(), false);
     
     Window.setTitle(page.getWindowTitle());
     Window.scrollTo(0, 0);
     
     setContent(page.getContent(), "content");
     setContent(page.getHeader(), "header");
     setContent(page.getNavigation(), "leftnav");
  }

  private void setContent(Composite content, String node) 
  {
      RootPanel contentRoot = RootPanel.get(node);
      contentRoot.clear();
      contentRoot.add(content);
  }
}
