package manostuart.client;

import java.sql.SQLException;
import java.util.ArrayList;

import manostuart.shared.data.Compra;
import manostuart.shared.data.Produto;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("Compra")
public interface CompraService extends RemoteService
{
	Compra comprar(ArrayList<Produto> prod, int total);
	Boolean setPagamentoEfetuado(String endereco);
	ArrayList<Compra> getComprasHistorico();
}
