package manostuart.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface CadastroProdutoServiceAsync
{
	void CadastroProduto (String nome, String marca, String categoria, String url,
			 int estoque, int preco, int peso, int dim_x, int dim_y, int dim_z,
			 AsyncCallback<Boolean> asyncCallback);
}