package manostuart.client;

import java.util.ArrayList;

import manostuart.shared.data.Produto;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface CarrinhoServiceAsync
{
	void GetCarrinho(AsyncCallback<ArrayList<Produto>> asyncCallback);
	
	void AddCarrinho(Produto prod, int num, AsyncCallback<Boolean> asyncCallback);
	

	void RefreshCarrinho(ArrayList<Produto> list, AsyncCallback<ArrayList<Produto>> asyncCallback);
}