package manostuart.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("cadastroproduto")
public interface CadastroProdutoService extends RemoteService
{
	Boolean CadastroProduto(String nome, String marca, String categoria, String url,
			 int estoque, int preco, int peso, int dim_x, int dim_y, int dim_z);   
}
