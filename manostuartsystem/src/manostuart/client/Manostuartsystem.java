package manostuart.client;


import manostuart.client.pages.PageDefs;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Window;

public class Manostuartsystem implements EntryPoint
{
   public void onModuleLoad() 
   {
      String token = Window.Location.getHash().length() == 0? 
            PageDefs.WelcomeToken : Window.Location.getHash();
      ContentContainer.getInstance().setPageByToken(token);

   }
}

