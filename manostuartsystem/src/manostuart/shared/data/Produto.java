package manostuart.shared.data;

import java.io.Serializable;

@SuppressWarnings("serial") // no no java no serialVersion of long for you
public class Produto implements Serializable{

	private String nome;

	private String marca;
	
	private String categoria;
	
	private String URLfoto;

	private int estoque;
	
	private int preco;

	private int peso;

	private int dim_x, dim_y, dim_z;
	
	private int qnt;
	

	
	public String getnome(){
		return nome;
	}
	
	public void setnome(String newValue){
		nome = newValue;
	}

	public String getmarca(){
		return marca;
	}

	public void setmarca(String newMarca){
		marca = newMarca;
	}
	
	public String getcategoria(){
		return categoria;
	}
	
	public void setcategoria(String newValue){
		categoria = newValue;
	}
	
	public String getURLfoto(){
		return URLfoto;
	}
	
	public void setURLfoto(String newValue){
		URLfoto = newValue;
	}

	public int getestoque(){
		return estoque;
	}

	public void setestoque(int newEstoque){
		estoque = newEstoque;
	}
	
	public int getpreco(){
		return preco;
	}
	
	public void setpreco(int newValue){
		preco = newValue;
	}
	
	public int getpeso(){
		return peso;
	}
	
	public void setpeso(int newPeso){
		peso = newPeso;
	}

	public int getdim_x(){
		return dim_x;
	}

	public void setdim_x(int newDim_x){
		dim_x = newDim_x;
	}

	public int getdim_y(){
		return dim_y;
	}

	public void setdim_y(int newDim_y){
		dim_y = newDim_y;
	}

	public int getdim_z(){
		return dim_z;
	}

	public void setdim_z(int newDim_z){
		dim_z = newDim_z;
	}

	public int getqnt() {
		return qnt;
	}

	public void setqnt(int qnt) {
		this.qnt = qnt;
	}


}