package manostuart.shared.data;
import java.io.Serializable;

@SuppressWarnings("serial") // no no java no serialVersion of long for you
public class Usuario implements Serializable{

	private String login;

	private String senha;
	
	private String nome;

	private String CPF;
	
	private String RG;

	private java.util.Date nascimento;

	private String sexo;	
	
	private boolean admin;

	
	public String getlogin(){
		return login;
	}
	
	public void setlogin(String newLogin){
		login = newLogin;
	}

	public String getsenha(){
		return senha;
	}

	public void setsenha(String newSenha){
		senha = newSenha;
	}

	public String getnome(){
		return nome;
	}

	public void setnome(String newNome){
		nome = newNome;
	}
	
	public String getCPF(){
		return CPF;
	}
	
	public void setCPF(String newValue){
		CPF = newValue;
	}
	
	
	public String getRG(){
		return RG;
	}
	
	public void setRG(String newRG){
		RG = newRG;
	}

	public java.util.Date getnascimento(){
		return nascimento;
	}

	public void setnascimento(java.util.Date newNascimento){
		nascimento = newNascimento;
	}

	public String getsexo(){
		return sexo;
	}

	public void setsexo(String newSexo){
		sexo = newSexo;
	}

	public boolean isadmin() {
		return admin;
	}

	public void setadmin(boolean admin) {
		this.admin = admin;
	}
}