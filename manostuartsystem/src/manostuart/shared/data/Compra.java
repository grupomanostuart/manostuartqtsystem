package manostuart.shared.data;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

@SuppressWarnings("serial") // no no java no serialVersion of long for you
public class Compra implements Serializable{

	private Usuario usr;
	
	private int valor;
	
	private int id;

	private boolean pago;
	
	private String data;
	
	private ArrayList<Produto> prods = new ArrayList<Produto>();
	
	private String endereco;
	

	
	public Usuario getusr(){
		return usr;
	}
	
	public void setusr(Usuario newValue){
		usr = newValue;
	}
	
	public int getvalor(){
		return valor;
	}
	
	public void setvalor(int newValue){
		valor = newValue;
	}
	
	public String getdata(){
		return data;
	}
	
	public void setdata(Date newValue){
		data = newValue.toString();
	}
	
	public void setdata(String newValue){
		data = newValue;
	}
	
	public ArrayList<Produto> getprods(){
		return prods;
	}
	
	public boolean addProdLista(Produto p){
		return prods.add(p);
	}
	
	public void setprods(ArrayList<Produto> newValue){
		prods = newValue;
	}

	public boolean getpago() {
		return pago;
	}

	public void setpago(boolean pago) {
		this.pago = pago;
	}

	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	public String getendereco() {
		return endereco;
	}

	public void setendereco(String endereco) {
		this.endereco = endereco;
	}
}
